# Import Python Packages
import os
import math
import numpy as np
from wetb.fatigue_tools.fatigue import rainflow_astm, rainflow_windap, cycle_matrix, cycle_matrix2
from wetb.prepost.windIO import LoadResults

class Markov(object):
    """
    This Class contains functions for processing HAWC2 time series to extract Markov Matrices
    """
    
    def __init__(self, path, chs_tranformation, bin_size):
        """Class initialization"""

        self.path = path
        self.child_dir = next(os.walk(path))[1]

        if not chs_tranformation:
            # but only take the channels that where not missing
            print('    ERROR: channel list for directional transformation is empty!')

        self.chs_tranformation = chs_tranformation

        self.bin_size = bin_size


    def build_transformation_ts(self, res, cname, sector=30):
        """
        Function for building the transformed time series for the directional Markov matrices
        """

        sectors = np.arange(0, 360, sector)
        sig_transformation = np.ndarray((len(res.sig[:,0]),\
                                         len(self.chs_tranformation)*len(sectors)))
        ch_tags = []

        for j, chs in enumerate(self.chs_tranformation):
            for ns, sec in enumerate(sectors):
                sig_res = np.ndarray((len(res.sig[:,0]), len(chs)))
                lab = 'alpha' + str(sec)
                no_channel = False
                for i, ch in enumerate(chs):
                    check = ''
                    if i > 1:
                        print('    z-coordinate not needed in transformations')
                        continue
                    # if the channel does not exist, zet to zero
                    try:
                        chi = res.ch_dict[ch]['chi']
                        sig_res[:,i] = res.sig[:,chi]
                        no_channel = False
                    except KeyError:
                        no_channel = True
                    check = ch.split('-')[-1]
                    if i == 0 and check != 'x':
                        print('    ERROR: first coordinate has to be x' +\
                              'to perform transformations correctly!')
                        break
                    elif i == 1 and check != 'y':
                        print('    ERROR: second coordinate has to be y' +\
                              'to perform transformations correctly!')
                        break
                name = '-'.join(ch.split('-')[:-1] + [lab])
                # when on of the components do no exist, we can not calculate
                # the transformations!
                if no_channel:
                    rpl = (name, cname)
                    print('    missing channel, no transformations for: %s, %s' % rpl)
                    continue
                else:
                    ch_tags.append(name)
                sig_transformation[:,ns+(j*len(sectors))] = np.sin(np.deg2rad(sec))*sig_res[:,0] -\
                                                             np.cos(np.deg2rad(sec))*sig_res[:,1]

        return sig_transformation, ch_tags


    def save_markov_matrix(self, dlc_path, sim_path, markov_mat, signal_name):
        """
        Function for saving the directional Markov matrices
        """

        prepost_path = os.path.join('prepost-data/markov', dlc_path)

        markov_dir = os.path.join(prepost_path, sim_path)

        try:
            os.makedirs(markov_dir)
        except:
            pass

        markov_file = os.path.join(markov_dir, signal_name)

        np.savetxt(markov_file + '.txt', markov_mat)


    def build_markov_matrix(self):
        """
        Function for building the directional Markov matrices
        """

        for p in self.child_dir:
            res_path = os.path.join(self.path, p)
            res_list = ['.'.join(f.split('.')[:-1]) for f in os.listdir(res_path) if f.endswith('.sel')]

            for nf, f in enumerate(res_list):
                sim = LoadResults(res_path, f)
                sigs, tags = self.build_transformation_ts(sim, f)

                print('Processing Markov matrices for %s, case %i of %i' % (f, nf+1, len(res_list)))

                for i, tag in enumerate(tags):
                    min_bin = sigs[:, i].min()
                    max_bin = sigs[:, i].max()

                    ampl_bins = np.arange(self.bin_size, abs(max_bin-min_bin) + self.bin_size*2, self.bin_size) - self.bin_size/2
                    mean_bins = np.arange(int(math.floor(min_bin / self.bin_size)) * self.bin_size,
                                          int(math.ceil(max_bin / self.bin_size)) * self.bin_size, self.bin_size) - self.bin_size/2

                    # Get the cycle matrix
                    try:
                        matrix, _, ampl_edges, _, mean_edges =\
                            cycle_matrix(sigs[:, i], ampl_bins, mean_bins, rainflow_func=rainflow_windap)
                        
                        ampl_bin_mean = (ampl_edges[:-1] + ampl_edges[1:])/2
                        mean_bin_mean = (mean_edges[:-1] + mean_edges[1:])/2
                        mean_bin_mean = np.append(0, mean_bin_mean)
                        
                        markov = np.insert(np.insert(matrix, 0, ampl_bin_mean, axis=1), 0, mean_bin_mean, axis=0)

                        markov = np.delete(markov,np.where(~markov[:,1:].any(axis=1))[0], axis=0)
                        markov = np.delete(markov,np.where(~markov[1:,:].any(axis=0))[0], axis=1)
                    except:
                        print('Signal is constant...Markov matrix set to zero!')
                        markov = np.zeros((2,2))

                    if np.size(markov) == 0:
                        markov = np.zeros((2,2))

                    self.save_markov_matrix(p, f, markov, tag)


if __name__ == '__main__':
    pass
